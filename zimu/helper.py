import chardet


def check_str_contain_chinese(check_str, encoding='utf-8'):
    for ch in check_str.decode(encoding, 'ignore'):
        if u'\u4e00' <= ch <= u'\u9fff':
            return True
        if u'\uff00' <= ch <= u'\uffef':
            return True
    return False


def check_file_contain_chinese(file_name, line_limit=10):
    f = file(file_name)

    for line_no in range(0, line_limit):
        line = f.readline()
        encoding = chardet.detect(line)['encoding']
        print 'encoding', encoding
        if check_str_contain_chinese(line, encoding):
            return True

    return False


def check_str_contain_other_foreign_language(check_str, encoding='utf-8'):
    for ch in check_str.decode(encoding, 'ignore'):
        if ch > u'\uffef':
            return True
        if u'\uff00' > ch > u'\u9fff':
            return True
        if ch < u'\u4e00' and ord(ch) > 128:
            return True
    return False


def check_file_contain_other_foreign_language(file_name, line_limit=10):
    f = file(file_name)
    for line_no in range(0, line_limit):
        line = f.readline()
        encoding = chardet.detect(line)['encoding']
        print 'encoding', encoding
        if check_str_contain_other_foreign_language(line, encoding):
            return True

    return False


def download_subtitle(movie_path):
    # to do: cope with subliminal to get the subtitle.
    pass