import os
import shutil
import tempfile

from django.test import TestCase
from django.test.client import Client
from django.urls import reverse

from naszoo import settings
from ying.models import MovieFolder, Movie, FolderVideo
from zimu.helper import check_file_contain_chinese, check_file_contain_other_foreign_language
from zimu.models import VideoSub
from zimu.shooter import download_subtitle


class ShooterTest(TestCase):
    __test_avi_file = None
    __test_folder = None

    def setUp(self):
        self.__test_folder = tempfile.mkdtemp()
        avi_file = 'testidx.avi'
        self.__test_avi_file = os.path.join(self.__test_folder, avi_file)
        print 'test file:', self.__test_avi_file
        shutil.copyfile(os.path.join(os.getcwd(), 'zimu', 'test', avi_file), self.__test_avi_file)

    def test_shooter(self):
        subtitle_files = download_subtitle(self.__test_avi_file)
        print 'subtitle_files:', subtitle_files
        self.assertTrue(len(subtitle_files) > 1)


class HelperTest(TestCase):
    @staticmethod
    def get_test_folder():
        return os.path.join(os.getcwd(), 'zimu', 'test', 'media', 'movie', 'This.Is.It.2009.720p.BluRay.x264-MgB')

    def test_chinese_gb2312(self):
        is_chinese = check_file_contain_chinese(os.path.join(self.get_test_folder(), 'chinese_gb2312.srt'))
        self.assertTrue(is_chinese)
        pass

    def test_chinese_uft8(self):
        is_chinese = check_file_contain_chinese(os.path.join(self.get_test_folder(), 'chinese_utf8.srt'))
        self.assertTrue(is_chinese)
        pass

    def test_chinese_uft8_bom(self):
        is_chinese = check_file_contain_chinese(os.path.join(self.get_test_folder(), 'chinese_utf8_bom.srt'))
        self.assertTrue(is_chinese)
        pass

    def test_english(self):
        is_chinese = check_file_contain_chinese(os.path.join(self.get_test_folder(), 'english.idx'))
        self.assertFalse(is_chinese)
        pass

    def test_check_file_contain_chinese(self):
        is_chinese = check_file_contain_chinese(os.path.join(self.get_test_folder(), 'romania.srt'))
        self.assertFalse(is_chinese)
        pass

    def test_check_file_contain_other_foreign_language(self):
        is_other_foreign = check_file_contain_other_foreign_language(
            os.path.join(self.get_test_folder(), 'romania.srt'))
        self.assertTrue(is_other_foreign)
        pass

    def test_check_file_contain_other_foreign_language2(self):
        is_other_foreign = check_file_contain_other_foreign_language(
            os.path.join(self.get_test_folder(), 'chinese_gb2312.srt'))
        self.assertFalse(is_other_foreign)
        pass


class ViewTest(TestCase):
    __folder = None
    __movie = None
    __video = None

    def setUp(self):
        self.__movie = Movie(name='This.Is.It.2009.720p.BluRay.x264-MgB', year=2009)
        self.__movie.save()

        self.__folder = MovieFolder(name='This.Is.It.2009.720p.BluRay.x264-MgB')
        self.__folder.movie = self.__movie
        self.__folder.save()

        self.__video = FolderVideo("testidx.avi")
        self.__video.folder = self.__folder
        self.__video.save()

        settings.MEDIA_ROOT = os.path.join(settings.BASE_DIR, 'zimu', 'test', 'media')

    def test_zimu_scan_folder_id(self):
        client = Client()
        response = client.get(reverse('zimu_scan'), {'folder_id': self.__folder.id})
        self.assertTrue(response.status_code == 200)
        subs = VideoSub.objects.filter(video=self.__video)
        self.assertTrue(subs is not None)
        self.assertTrue(len(subs) > 0)

    def test_zimu_scan_movie_id(self):
        client = Client()
        response = client.get(reverse('zimu_scan'), {'movie_id': self.__movie.id})
        self.assertTrue(response.status_code == 200)
