from __future__ import unicode_literals

from django.db import models

from ying.models import FolderVideo


class VideoSub(models.Model):
    video = models.ForeignKey(FolderVideo)
    link = models.CharField(max_length=100)
    delay = models.IntegerField(null=True, blank=True)
    language = models.CharField(max_length=20)
    name = models.CharField(max_length=100, null=True, blank=True)
    inode = models.CharField(max_length=100, null=True, blank=True)

    def __unicode__(self):
        if self.name:
            return self.name
        return self.link
