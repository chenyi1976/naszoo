import os

from django.http.response import Http404
from django.shortcuts import render_to_response, get_object_or_404

from naszoo import settings
from ying.models import Movie, MovieFolder
from zimu.helper import check_file_contain_other_foreign_language, check_file_contain_chinese
from zimu.models import VideoSub



def zimu_scan(request):
    movie_id = request.GET.get("movie_id")
    movie_folder = None
    if not movie_id:
        folder_id = request.GET.get("folder_id")
        movie_folder = get_object_or_404(MovieFolder, pk=folder_id)
    else:
        movie = get_object_or_404(Movie, pk=movie_id)
        movie_folders = movie.moviefolder_set.all()
        if movie_folders:
            movie_folder = movie_folders[0]

    if not movie_folder:
        raise Http404("valid movie_id or folder_id is needed.")

    folder = os.path.join(settings.MEDIA_ROOT, 'movie', movie_folder.name)
    #
    # if os.path.exists(folder):
    #     files = [f for f in os.listdir(folder)
    #              if os.path.isfile(os.path.join(folder, f)) and f.endswith(('.mov', '.mp4', '.mkv', 'avi'))
    #              and (os.path.getsize(os.path.join(folder, f)) > 100000 or settings.DEBUG)]
    # else:
    #     files = []

    folder_videos = movie_folder.foldervideo_set.all()

    success_dict = {}
    fail_list = []
    files = []
    for folder_video in folder_videos:
        files.append(folder_video.name)
        full_path = os.path.join(folder, folder_video.name)
        if not os.path.exists(full_path):
            fail_list.append(folder_video.name)
            continue

        subtitle_files = download_subtitle(full_path)
        if subtitle_files:
            success_dict[folder_video.name] = subtitle_files
            # todo: remove all bad language subtitle file here.
            for subtitle_file in subtitle_files:
                sub_file_name = subtitle_file[0]
                sub_link = subtitle_file[1]
                sub_delay = subtitle_file[2]

                f = os.path.join(folder, sub_file_name)
                if check_file_contain_other_foreign_language(f):
                    lang = 'other'
                elif check_file_contain_chinese(f):
                    lang = 'zh'
                else:
                    lang = 'en'
                # todo: save VideoSub here.

            video_sub = VideoSub(video=folder_video, delay=sub_delay, link=sub_link, name=sub_file_name,
                                 language=lang, inode=os.stat(f).st_ino)
            video_sub.save()
        else:
            fail_list.append(folder_video.name)

    message_list = ['files:' + ','.join(files),
                    'success:' + ','.join(success_dict),
                    'fail:' + ','.join(fail_list)]
    context = {'message_list': message_list, 'files': files, 'success_dict': success_dict, 'fail_list': fail_list}

    return render_to_response('naszoo/message_list.html', context)
