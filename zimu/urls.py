from django.conf.urls import url

from zimu import views

urlpatterns = [
    url(r'^zimu/scan$', views.zimu_scan, name='zimu_scan'),
]
