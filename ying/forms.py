from django import forms

from ying.helper import get_tmdb_movie, save_movie
from ying.models import Movie, MovieFolder


class MovieFolderForm(forms.ModelForm):
    tmdb_id = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        initial = kwargs.get('initial')
        instance = kwargs.get('instance')
        if instance and initial:
            initial['tmdb_id'] = instance.movie.tmdb_id

        super(MovieFolderForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(MovieFolderForm, self).clean()
        tmdb_id = cleaned_data.get("tmdb_id", "")
        if tmdb_id:
            try:
                movie = Movie.objects.get(tmdb_id=tmdb_id)
                cleaned_data['movie'] = movie
            except Movie.DoesNotExist:
                tmdb_movie = get_tmdb_movie(tmdb_id)
                if tmdb_movie:
                    movie = save_movie(tmdb_movie)
                    cleaned_data['movie'] = movie
        return cleaned_data

    class Meta:
        model = MovieFolder
        fields = ['name', 'inode', 'movie']
