from django.db import models
from taggit.managers import TaggableManager
from sorl.thumbnail import ImageField


class Category(models.Model):
    name = models.CharField(max_length=50)
    tmdb_id = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return self.name


class Movie(models.Model):
    name = models.CharField(max_length=500)
    org_name = models.CharField(max_length=500)
    poster = ImageField(upload_to='movie_image')
    backdrop = ImageField(upload_to='movie_image')
    tmdb_id = models.IntegerField(blank=True, null=True)
    tmdb_popularity = models.FloatField(blank=True, null=True)
    tmdb_score = models.FloatField(blank=True, null=True)
    summary = models.TextField(max_length=5000, blank=True, null=True)
    year = models.IntegerField()
    category = models.ManyToManyField(Category)
    tags = TaggableManager(blank=True)
    comment = models.TextField(max_length=5000, blank=True, null=True)

    def __unicode__(self):
        encode_name = self.org_name.encode('ascii', 'ignore')
        if not encode_name:
            encode_name = self.name.encode('ascii', 'ignore')
        return '{}:{},{}'.format(self.tmdb_id, encode_name, self.year)


class MovieImage(models.Model):
    movie = models.ForeignKey(Movie)
    image = ImageField()
    image_type = models.IntegerField(blank=True, null=True)
    lang = models.CharField(max_length=20, blank=True, null=True)


class MovieFolder(models.Model):
    name = models.CharField(max_length=500)
    inode = models.CharField(max_length=20, null=True, blank=True)
    movie = models.ForeignKey(Movie, blank=True, null=True)
    deleted = models.BooleanField(default=False)

    def __unicode__(self):
        status = "[D]" if self.deleted else ""
        return status + self.name


class FolderVideo(models.Model):
    folder = models.ForeignKey(MovieFolder)
    name = models.CharField(max_length=500)
    inode = models.CharField(max_length=20, null=True, blank=True)
    size = models.PositiveIntegerField(null=True, blank=True)
    duration = models.PositiveIntegerField(null=True, blank=True)
    height = models.PositiveIntegerField(null=True, blank=True)
    width = models.PositiveIntegerField(null=True, blank=True)
    media_info = models.TextField(max_length=2000, null=True, blank=True)

    def __unicode__(self):
        return self.name
