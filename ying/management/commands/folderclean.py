from __future__ import print_function

from django.core.management import BaseCommand

from ying.helper import *


class Command(BaseCommand):
    help = 'Clean Folder'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        print('Begin Clean Folder')
        objects_all = MovieFolder.objects.all()
        folder_count = len(objects_all)
        objects_all.delete()
        print('{} folders have been removed'.format(folder_count))
