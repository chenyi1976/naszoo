from __future__ import print_function

from django.core.management import BaseCommand

from ying.helper import *


class Command(BaseCommand):
    help = 'Scan Folder'

    def add_arguments(self, parser):
        parser.add_argument('--force', type=bool, default=False)
        pass

    def handle(self, *args, **options):
        force_refresh = options['force']
        print('begin scan folder')
        folders = scan_movie_folder(settings.MOVIE_ROOT, force_refresh=force_refresh, command_mode=True)
        print('{} folders scanned'.format(len(folders)))
