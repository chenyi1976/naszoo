from __future__ import print_function

from django.core.management import BaseCommand

from ying.helper import *


class Command(BaseCommand):
    help = 'Clean Movie'

    def add_arguments(self, parser):
        # parser.add_argument('--force', type=bool, default=False)
        pass

    def handle(self, *args, **options):
        print('Begin Clean Movie')
        objects_all = Movie.objects.exclude(id__in=MovieFolder.objects.all().values('movie'))
        folder_count = len(objects_all)
        print('{} Movie need to be removed'.format(folder_count))
        objects_all.delete()
        print('{} Movie have been removed'.format(folder_count))
