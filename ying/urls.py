from django.conf.urls import url, include
from django.contrib.auth.views import login
from django.views.generic import RedirectView

from ying import views

urlpatterns = [
    url(r'^accounts/login/$', login, {'template_name': 'chemist/login.html'}),
    # url(r'^accounts/edit$',
    #     views.account_edit,
    #     name="account_edit"),
    url(r'^accounts/', include('django.contrib.auth.urls')),

    url(r'^$', RedirectView.as_view(url='/movie/search', permanent=True), name='index'),

    url(r'^movie_folder$', views.MovieFolderListView.as_view()),

    url(r'^movie_folder/scan$', views.movie_scan),
    url(r'^movie_folder/no_movie$', views.FolderWithoutMovieListView.as_view(), name="folder_without_movie"),

    url(r'^movie_folder/detail/(?P<pk>[\w-]+)$', views.MovieFolderUpdateView.as_view(), name="folder_update"),
    url(r'^movie_folder/play/(?P<pk>[\w-]+)$', views.folder_play, name="folder_play"),


    url(r'^movie/detail/(?P<pk>[\w-]+)$', views.MovieDetailView.as_view(), name="movie_detail"),
    url(r'^movie/update/(?P<pk>[\w-]+)$', views.MovieUpdateView.as_view(), name="movie_update"),
    url(r'^movie/delete/(?P<pk>[\w-]+)$', views.MovieDeleteView.as_view(), name="movie_delete_confirm"),

    url(r'^movie/exist$', views.ExistMovieSearchListView.as_view(), name="movies_exist"),
    url(r'^movie/exist/g$', views.MovieSearchGalleryListView.as_view(), name="movies_exist_gallery"),
    url(r'^movie/search', views.MovieSearchListView.as_view(), name="movies_search"),

    url(r'^category$', views.CategoryListView.as_view()),
    url(r'^category/detail/(?P<pk>[\w-]+)$', views.CategoryUpdateView.as_view()),
]
