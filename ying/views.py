import os

from django.db.models import Q
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic.edit import UpdateView, DeleteView
from taggit.models import Tag

from naszoo import settings
from ying.forms import MovieFolderForm
from ying.helper import scan_movie_folder
from ying.models import Movie, Category, MovieFolder


def movie_scan(request):
    scan_movie_folder(settings.MOVIE_ROOT)
    return redirect('/')


def folder_play(request, pk):
    movie_folder = MovieFolder.objects.get(pk=pk)
    folder = os.path.join(settings.MOVIE_ROOT, movie_folder.name)
    files = os.listdir(folder)
    videos = []
    subtitles = []
    for f in files:
        if f.lower().endswith('mp4'):
            videos.append(settings.STATIC_URL + settings.MOVIE_FOLDER + '/' + movie_folder.name + "/" + f)
        elif f[-3:].lower() in ('srt', 'ass'):
            subtitles.append(settings.STATIC_URL + settings.MOVIE_FOLDER + '/' + movie_folder.name + "/" + f)
    context = {'videos': videos,
               'subtitles': subtitles,
               'movie_folder': movie_folder}

    return render(request, 'ying/folder_play.html', context)


class FolderWithoutMovieListView(ListView):
    model = MovieFolder
    paginate_by = 20

    def get_queryset(self):
        return MovieFolder.objects.filter(movie__isnull=True)


class MovieFolderListView(ListView):
    model = MovieFolder
    paginate_by = 20

    def get_queryset(self):
        return MovieFolder.objects.order_by('name').filter(deleted=False)


class MovieFolderUpdateView(UpdateView):
    model = MovieFolder
    form_class = MovieFolderForm
    template_name = "ying/form_details.html"

    def get_success_url(self):
        return reverse('folder_without_movie')


class CategoryListView(ListView):
    model = Category


class CategoryUpdateView(UpdateView):
    model = Category
    fields = ['name', 'tmdb_id']
    template_name = "ying/form_details.html"


class MovieDetailView(DetailView):
    model = Movie


class MovieUpdateView(UpdateView):
    model = Movie
    fields = ['name', 'org_name', 'year', 'category', 'tags', 'summary']
    template_name = "ying/form_details.html"

    def get_success_url(self):
        return reverse('movies_exist')


class MovieDeleteView(DeleteView):
    model = Movie
    template_name = "ying/delete_confirm.html"

    def get_success_url(self):
        return reverse('movies_exist')


class MovieSearchListView(ListView):
    model = Movie
    paginate_by = 20
    template_name = 'ying/movie_list.html'
    only_downloaded = False
    q = None

    def get_queryset(self):
        tag_name = self.request.GET.getlist("tag_name", None)
        if not tag_name:
            tag_name = self.request.POST.getlist("tag_name", None)
        category_name = self.request.GET.getlist("category_name", None)
        if not category_name:
            category_name = self.request.POST.getlist("category_name", None)
        year = self.request.GET.get("year", None)
        if not year:
            year = self.request.POST.get("year", None)
        score_min = self.request.GET.get("score_min", None)
        if not score_min:
            score_min = self.request.POST.get("score_min", None)
        score_max = self.request.GET.get("score_max", None)
        if not score_max:
            score_max = self.request.POST.get("score_max", None)
        q = self.request.GET.get("q", None)
        if not q:
            q = self.request.POST.get("q", None)

        movies = Movie.objects.all()
        if self.only_downloaded:
            movies = movies.filter(id__in=MovieFolder.objects.all().values('movie'))

        if tag_name:
            movies = movies.filter(tags__name__in=tag_name)
        if category_name:
            movies = movies.filter(category__name__in=category_name)
        if year:
            movies = movies.filter(year=year)
        if score_min:
            movies = movies.filter(tmdb_score__gte=score_min)
        if score_max:
            movies = movies.filter(tmdb_score__lte=score_max)
        if q:
            movies = movies.filter(Q(name__contains=q) | Q(org_name__contains=q))
        return movies.distinct()

    def get_context_data(self, **kwargs):
        context = super(MovieSearchListView, self).get_context_data(**kwargs)
        tag_name = self.request.GET.getlist("tag_name", None)
        if not tag_name:
            tag_name = self.request.POST.getlist("tag_name", None)
        category_name = self.request.GET.getlist("category_name", None)
        if not category_name:
            category_name = self.request.POST.getlist("category_name", None)
        year = self.request.GET.get("year", None)
        if not year:
            year = self.request.POST.get("year", None)
        score_min = self.request.GET.get("score_min", None)
        if not score_min:
            score_min = self.request.POST.get("score_min", None)
        score_max = self.request.GET.get("score_max", None)
        if not score_max:
            score_max = self.request.POST.get("score_max", None)

        only_downloaded = self.request.GET.get("only_downloaded", None)
        if not only_downloaded:
            only_downloaded = self.request.POST.get("only_downloaded", None)

        if only_downloaded is not None:
            if only_downloaded in ['false', 'False', 'False', 'NO', 'no', 'No']:
                only_downloaded = False
            else:
                only_downloaded = bool(only_downloaded)
            self.only_downloaded = only_downloaded

        q = self.request.GET.get("q", None)
        if not q:
            q = self.request.POST.get("q", None)

        if tag_name:
            context['tag_name'] = tag_name
        if category_name:
            context['category_name'] = category_name
        if year:
            context['year'] = year
        if score_min:
            context['score_min'] = score_min
        if score_max:
            context['score_max'] = score_max
        if only_downloaded is not None:
            context['only_downloaded'] = only_downloaded
        if q is not None:
            context['q'] = q

        context['category_list'] = Category.objects.all()
        context['tag_list'] = Tag.objects.all()

        return context


class ExistMovieSearchListView(MovieSearchListView):
    only_downloaded = True


class MovieSearchGalleryListView(MovieSearchListView):
    only_downloaded = True
    template_name = 'ying/movie_list_g.html'
