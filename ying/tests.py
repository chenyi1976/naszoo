import shutil
import tempfile

from django.test import TestCase, Client

from ying.helper import *
from ying.models import *


class E2ECase(TestCase):
    client = Client()

    def test_pages(self):
        test_urls = ('/movie_folder', '/movie_folder/no_movie', '/movie/exist', '/category')
        for test_url in test_urls:
            response = self.client.get(test_url)
            self.assertEqual(response.status_code, 200)


class HelperTest(TestCase):
    def setUp(self):
        genre_dict = {'Action': 28,
                      'Adventure': 12,
                      'Animation': 16,
                      'Comedy': 35,
                      'Crime': 80,
                      'Documentary': 99,
                      'Drama': 18,
                      'Family': 10751,
                      'Fantasy': 14,
                      'History': 36,
                      'Horror': 27,
                      'Music': 10402,
                      'Mystery': 9648,
                      'Romance': 10749,
                      'Science Fiction': 878,
                      'TV Movie': 10770,
                      'Thriller': 53,
                      'War': 10752,
                      'Western': 37,
                      }
        for n in genre_dict:
            Category(name=n, tmdb_id=genre_dict[n]).save()

    def test_analyse_movie_folder(self):
        movie_folder = analyse_movie_folder('Tinker.Tailor.Soldier.Spy.2011.720p.BRRip.1.1GB.MkvCage')
        self.assertIsNotNone(movie_folder)
        self.assertTrue(len(movie_folder) == 2)
        self.assertEqual(movie_folder[0], 'Tinker Tailor Soldier Spy')
        self.assertEqual(movie_folder[1], 2011)

    def test_analyse_movie_folder2(self):
        movie_folder = analyse_movie_folder('SomeRandomJunkNonsense')
        self.assertIsNotNone(movie_folder)
        self.assertTrue(len(movie_folder) == 2)
        self.assertEqual(movie_folder[0], 'SomeRandomJunkNonsense')
        self.assertEqual(movie_folder[1], '')

    def test_analyse_movie_folder3(self):
        movie_folder = analyse_movie_folder('Fading.Gigolo.2013.1080p.BluRay.x264.YIFY')
        self.assertIsNotNone(movie_folder)
        self.assertTrue(len(movie_folder) == 2)
        self.assertEqual(movie_folder[0], 'Fading Gigolo')
        self.assertEqual(movie_folder[1], 2013)

    def test_analyse_movie_folder4(self):
        movie_folder = analyse_movie_folder('Fading.Gigolo.2013')
        self.assertIsNotNone(movie_folder)
        self.assertTrue(len(movie_folder) == 2)
        self.assertEqual(movie_folder[0], 'Fading Gigolo')
        self.assertEqual(movie_folder[1], 2013)

    def test_analyse_movie_folder5(self):
        movie_folder = analyse_movie_folder('Argo.Extended.Cut.2012.1080p')
        self.assertIsNotNone(movie_folder)
        self.assertTrue(len(movie_folder) == 2)
        self.assertEqual(movie_folder[0], 'Argo')
        self.assertEqual(movie_folder[1], 2012)

    def test_search_tmdb_related_movie(self):
        related_movie_ids = search_tmdb_related_movie(13576)
        self.assertIsNotNone(related_movie_ids)
        self.assertTrue(len(related_movie_ids) > 0)

        for related_people_id in related_movie_ids:
            related_movie_id = related_movie_ids[related_people_id]
            print(related_people_id)
            print(get_tmdb_people(related_people_id)['name'])
            print(related_movie_id)
            print(get_tmdb_movie(related_movie_id)['title'])
            # print('{} {}: {} {}'.format(get_tmdb_people(related_people_id)['name'], related_people_id,
            #       get_tmdb_movie(related_movie_id)['title'], related_movie_id))

    def test_get_tmdb_people(self):
        tmdb_people = get_tmdb_people(3)
        self.assertIsNotNone(tmdb_people)
        self.assertTrue('name' in tmdb_people)

    def test_search_movie(self):
        movie_infos = search_tmdb_movie('Tinker Tailor Soldier Spy')
        self.assertIsNotNone(movie_infos)
        self.assertTrue(len(movie_infos) > 0)
        print(movie_infos[0]['overview'])

    def test_search_movie2(self):
        movie_infos = search_tmdb_movie('SomeRandomJunkNonsense')
        self.assertIsNotNone(movie_infos)
        self.assertTrue(len(movie_infos) == 0)

    def test_search_save_movies(self):
        movie_infos = search_tmdb_movie('Tinker Tailor Soldier Spy')
        movies = save_movies(movie_infos)
        self.assertIsNotNone(movies)
        self.assertTrue(len(movies) > 0)

    def test_search_save_movie(self):
        movie_info = get_tmdb_movie(603)
        movie = save_movie(movie_info)
        self.assertIsNotNone(movie)
        self.assertTrue(type(movie) is Movie)

    def test_search_save_movie2(self):
        movie_info = get_tmdb_movie('xyz')
        self.assertIsNone(movie_info)

    def test_save_movie_images(self):
        tmdb_movie = get_tmdb_movie(603)
        save_movie(tmdb_movie)

        m_603 = Movie.objects.filter(tmdb_id=603)[0]

        images = save_movie_images(m_603)
        self.assertIsNotNone(images)
        self.assertTrue(len(images) > 0)
        self.assertTrue('posters' in images)
        self.assertIsNotNone(images['posters'])
        # self.assertTrue(len(images['posters']) > 0)

        self.assertTrue('backdrops' in images)
        self.assertIsNotNone(images['backdrops'])
        # self.assertTrue(len(images['backdrops']) > 0)

    def test_scan_movie_folder(self):
        temp_dir = tempfile.mkdtemp()

        print(temp_dir)

        folder_dict = {'The.Wolf.of.Wall.Street.2013.DVDSCR.XviD-BiDA': 123,
                       'Tinker.Tailor.Soldier.Spy.2011.720p.BRRip.1.1GB.MkvCage': 456}
        file_dict = {'The.Wolf.of.Wall.Street.2013.DVDSCR.XviD-BiDA': ['1.mp4', '2.mp4'],
                     'Tinker.Tailor.Soldier.Spy.2011.720p.BRRip.1.1GB.MkvCage': ['3.mp4', '4.mp4']}

        for folder_name in folder_dict:
            full_path = os.path.join(temp_dir, folder_name)
            os.mkdir(full_path)
            files = file_dict[folder_name]
            for f_name in files:
                with open(os.path.join(full_path, f_name), 'w') as f:
                    f.write('123')

        new_folders = scan_movie_folder(movie_dir=temp_dir)

        self.assertIsNotNone(new_folders)
        self.assertEqual(len(new_folders), len(folder_dict))
        for folder, d in zip(new_folders, folder_dict):
            self.assertEqual(folder.name, d)

        rename_dict = {'The.Wolf.of.Wall.Street.2013.DVDSCR.XviD-BiDA': {'2.mp4': '2x.mp4'},
                       'Tinker.Tailor.Soldier.Spy.2011.720p.BRRip.1.1GB.MkvCage': {'4.mp4': '4x.mp4'}}
        new_file_dict = {'The.Wolf.of.Wall.Street.2013.DVDSCR.XviD-BiDA': ['1.mp4', '2x.mp4'],
                         'Tinker.Tailor.Soldier.Spy.2011.720p.BRRip.1.1GB.MkvCage': ['3.mp4', '4x.mp4']}

        for folder_name in rename_dict:
            d = rename_dict[folder_name]
            for org_name in d:
                os.rename(os.path.join(temp_dir, folder_name, org_name),
                          os.path.join(temp_dir, folder_name, d[org_name]))

        scan_movie_folder(movie_dir=temp_dir, force_refresh=True)
        for folder_name in new_file_dict:
            folders = MovieFolder.objects.filter(name=folder_name)
            for folder in folders:
                ff = folder.foldervideo_set.all()
                for f in ff:
                    self.assertTrue(f.name in new_file_dict[folder.name])

        shutil.rmtree(temp_dir)

    def test_search_movies(self):
        folder_dict = {'The.Wolf.of.Wall.Street.2013.DVDSCR.XviD-BiDA': 123,
                       'Tinker.Tailor.Soldier.Spy.2011.720p.BRRip.1.1GB.MkvCage': 456}
        new_folders = search_movies(folder_dict)
        self.assertIsNotNone(new_folders)
        self.assertEqual(len(new_folders), len(folder_dict))
        self.assertTrue(type(new_folders[0]) is MovieFolder)

    def test_search_movies(self):
        folder_dict = {'The.Wolf.of.Wall.Street.2013.DVDSCR.XviD-BiDA': 123,
                       'Tinker.Tailor.Soldier.Spy.2011.720p.BRRip.1.1GB.MkvCage': 456}
        new_folders = search_movies(folder_dict)
        self.assertIsNotNone(new_folders)
        self.assertEqual(len(new_folders), len(folder_dict))
        self.assertTrue(type(new_folders[0]) is MovieFolder)

    def test_search_movies2(self):
        folder_dict = {'SomeRandomNonsense': 123,
                       'SomeRandomNonsense2': 456}
        new_folders = search_movies(folder_dict)
        self.assertIsNotNone(new_folders)
        self.assertTrue(len(new_folders) == 2)
        for folder in new_folders:
            self.assertTrue(folder.movie is None)

    def test_search_movies3(self):
        folder_dict = {'The.Wolf.of.Wall.Street.2013.DVDSCR.XviD-BiDA': 123}
        new_folders = search_movies(folder_dict)
        self.assertIsNotNone(new_folders)
        self.assertEqual(len(new_folders), len(folder_dict))
        self.assertTrue(type(new_folders[0]) is MovieFolder)
        folder = new_folders[0]
        folder.movie.name = 'SomeRandomNonsense'
        folder.movie.save()

        folder_dict = {'The.Wolf.of.Wall.Street.2013.DVDSCR.XviD-BiDA': 123}
        new_folders = search_movies(folder_dict)
        self.assertIsNotNone(new_folders)
        self.assertEqual(len(new_folders), len(folder_dict))
        self.assertEqual(new_folders[0].movie.name, 'SomeRandomNonsense')

        folder_dict = {'The.Wolf.of.Wall.Street.2013.DVDSCR.XviD-BiDA': 123}
        new_folders = search_movies(folder_dict, force_refresh=True)
        self.assertIsNotNone(new_folders)
        self.assertEqual(len(new_folders), len(folder_dict))
        self.assertNotEqual(new_folders[0].movie.name, 'SomeRandomNonsense')
