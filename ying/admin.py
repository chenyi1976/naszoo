from django.contrib import admin

from sorl.thumbnail.admin import AdminImageMixin
from ying.models import MovieFolder, Category, Movie, MovieImage, FolderVideo


class MyModelAdmin(AdminImageMixin, admin.ModelAdmin):
    pass


admin.site.register(Category)
admin.site.register(Movie)
admin.site.register(MovieFolder)
admin.site.register(MovieImage)
admin.site.register(FolderVideo)
