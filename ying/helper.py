import logging
import os
import urllib

import tmdbsimple as tmdb
from pymediainfo import MediaInfo
from requests import HTTPError

from naszoo import settings
from ying.models import Movie, Category, MovieImage, MovieFolder, FolderVideo


def search_tmdb_related_movie(tmdb_movie_id, count=5):
    casts = tmdb.Movies(tmdb_movie_id).credits()['cast']
    related_movie_ids = {}
    for cast in casts[:count]:
        cast_id = cast['cast_id']
        related_movies = tmdb.Discover().movie(with_cast=cast_id)['results']
        if related_movies is not None:
            if len(related_movies) > 0:
                related_movie_ids[cast_id] = related_movies[0]['id']
    return related_movie_ids


def get_tmdb_people(people_id):
    people = tmdb.People(people_id)
    try:
        response = people.info(language='zh-CN')
    except HTTPError:
        return None
    return response


def analyse_movie_folder(folder_name):
    folder_name = folder_name.replace('.', ' ')
    folder_name = folder_name.replace('_', ' ')
    folder_name = folder_name.replace('-', ' ')

    from guessit import guessit
    movie_dict = None
    try:
        movie_dict = guessit(folder_name)
    except:
        pass
    movie_title = folder_name
    movie_year = ''
    if movie_dict:
        if 'title' in movie_dict:
            movie_title = movie_dict['title']
        if 'year' in movie_dict:
            movie_year = movie_dict['year']
    return [movie_title, movie_year]


def search_tmdb_movie(name, year=None):
    search = tmdb.Search()
    response = search.movie(query=name, year=year, language='zh-CN')
    return response['results']


def get_tmdb_movie(tmdb_id):
    movie = tmdb.Movies(tmdb_id)
    try:
        response = movie.info(language='zh-CN')
    except HTTPError:
        return None
    return response


def save_movie(movie_info, update_existing=False):
    if movie_info is None:
        return None

    poster_info = movie_info['poster_path']
    title = movie_info['title']
    overview = movie_info['overview']
    release_date = movie_info['release_date']
    release_year = -1
    if release_date:
        try:
            release_year = int(release_date[:4])
        except ValueError:
            pass

    popularity = -1
    try:
        popularity = float(movie_info['popularity'])
    except ValueError:
        pass

    org_title = movie_info['original_title']
    org_title = org_title.encode('ascii', 'ignore')
    org_title = ''.join(
        c for c in org_title if c in '-_ abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')

    org_title_no_space = org_title.replace(' ', '')

    backdrop_info = movie_info['backdrop_path']
    movie_info['vote_count']
    movie_info['video']
    movie_info['adult']

    vote_average = -1
    try:
        vote_average = float(movie_info['vote_average'])
    except ValueError:
        pass

    movie_info['original_language']
    movie_id = movie_info['id']
    genre_ids = []
    if 'genre_ids' in movie_info:
        genre_ids = movie_info['genre_ids']
    elif 'genres' in movie_info:
        genre_list = movie_info['genres']
        for genre in genre_list:
            genre_ids.append(genre['id'])

    # Poster
    poster_path = None
    if poster_info:
        poster_url = 'https://image.tmdb.org/t/p/original' + poster_info
        poster_path = os.path.join(settings.MOVIE_IMAGE_FOLDER,
                                   "{}_{}_{}_Poster.jpg".format(org_title_no_space, release_year, movie_id))
        # poster_path = os.path.join('movie', "{}_{}_{}_Poster.jpg".format(org_title, release_year, movie_id))
        poster_full_path = os.path.join(settings.MEDIA_ROOT, poster_path)
        if not os.path.exists(poster_full_path):
            urllib.urlretrieve(poster_url, poster_full_path)
            # os.link(poster_path, )

    # Backdrop
    backdrop_path = None
    if backdrop_info:
        backdrop_url = 'https://image.tmdb.org/t/p/original' + backdrop_info
        backdrop_path = os.path.join(settings.MOVIE_IMAGE_FOLDER,
                                     "{}_{}_{}_Backdrop.jpg".format(org_title_no_space, release_year, movie_id))
        backdrop_full_path = os.path.join(settings.MEDIA_ROOT, backdrop_path)
        if not os.path.exists(backdrop_full_path):
            urllib.urlretrieve(backdrop_url, backdrop_full_path)

    existing_movie = Movie.objects.filter(tmdb_id=movie_id)

    if existing_movie:
        if not update_existing:
            return existing_movie[0]
        movie = existing_movie[0]
    else:
        movie = None

    if movie is None:
        movie = Movie()

    movie.tmdb_id = movie_id
    movie.name = title
    movie.org_name = org_title
    movie.year = release_year
    movie.summary = overview
    movie.poster = poster_path
    movie.backdrop = backdrop_path
    movie.tmdb_score = vote_average
    movie.tmdb_popularity = popularity

    movie.save()

    # Genre
    for genre_id in genre_ids:
        genres = Category.objects.filter(tmdb_id=genre_id)
        if genres:
            category = genres[0]
            movie.category.add(category)
        else:
            logging.warn('genre_id {} not found for {}'.format(genre_id, org_title))

    return movie


def save_movies(movie_infos, update_existing=False):
    movies = []
    for movie_info in movie_infos:
        movies.append(save_movie(movie_info, update_existing))
    return movies


def save_movie_images(movie):
    images = tmdb.Movies(movie.tmdb_id).images()
    backdrops = images['backdrops']

    backdrop_images = []
    for backdrop in backdrops:
        width = backdrop['width']
        lang = backdrop['iso_639_1']
        backdrop['height']
        backdrop['vote_average']
        backdrop['vote_count']
        backdrop['aspect_ratio']
        url_path = backdrop['file_path']
        image_name = url_path[1:]

        backdrop_url = 'https://image.tmdb.org/t/p/original' + url_path

        backdrop_path = os.path.join(settings.MOVIE_IMAGE_FOLDER,
                                     "{}_{}_{}_backdrop_{}".format(movie.org_name, movie.year, movie.tmdb_id, image_name))
        backdrop_full_path = os.path.join(settings.MEDIA_ROOT, backdrop_path)
        if os.path.exists(backdrop_full_path):
            logging.debug('already exist, ignore: {}'.format(url_path))
            continue

        logging.debug('retrieving {}'.format(url_path))
        urllib.urlretrieve(backdrop_url, backdrop_full_path)

        MovieImage(movie=movie, image=backdrop_path, image_type=0, lang=lang).save()

        backdrop_images.append(backdrop_path)

    posters = images['posters']

    poster_images = []
    for poster in posters:
        width = poster['width']
        lang = poster['iso_639_1']
        poster['height']
        poster['vote_average']
        poster['vote_count']
        poster['aspect_ratio']
        url_path = poster['file_path']
        image_name = url_path[1:]

        poster_url = 'https://image.tmdb.org/t/p/original' + url_path
        poster_path = os.path.join(settings.MOVIE_IMAGE_FOLDER,
                                   "{}_{}_{}_poster_{}".format(movie.org_name, movie.year, movie.tmdb_id, image_name))
        poster_full_path = os.path.join(settings.MEDIA_ROOT, poster_path)
        if os.path.exists(backdrop_full_path):
            logging.debug('already exist, ignore: {}'.format(url_path))
            continue

        logging.debug('retrieving {}'.format(url_path))
        urllib.urlretrieve(poster_url, poster_full_path)

        MovieImage(movie=movie, image=poster_path, image_type=1, lang=lang).save()

        poster_images.append(poster_path)

    return {"backdrops": backdrop_images, "posters": poster_images}


def scan_movie_folder(movie_dir, force_refresh=False, command_mode=False):
    if not movie_dir:
        return []
    folder_names = os.listdir(movie_dir)
    folder_dict = {}
    for f in folder_names:
        full_path = os.path.join(movie_dir, f)
        if not os.path.isdir(full_path):
            folder_names.remove(f)
        else:
            mp4files = [sf for sf in os.listdir(full_path)
                        if sf.endswith('.mp4') or sf.endswith('.mkv') or sf.endswith('m4v')]
            if mp4files:
                stats = os.stat(full_path)
                folder_dict[f] = stats.st_ino

    MovieFolder.objects.all().update(deleted=True)

    folders = search_movies(folder_dict, force_refresh, movie_dir, command_mode=command_mode)
    return folders


def search_movies(folder_dict, force_refresh=False, root_folder=None, command_mode=False):
    # if not force_refresh:
    #     movie_folders = MovieFolder.objects.filter(movie__isnull=False)
    #     for folder_name in movie_folders:
    #         if folder_name.name in folder_dict:
    #             del folder_dict[folder_name.name]

    movie_folders = []
    index = 0
    for folder_name in folder_dict:
        index += 1
        if command_mode:
            print('updating {}/{}:{}'.format(index, len(folder_dict), folder_name))
        try:
            movie_folder = MovieFolder.objects.get(name=folder_name)
            movie_folder.deleted = False
            movie_folder.save()
            if movie_folder.movie:
                if not force_refresh:
                    movie_folders.append(movie_folder)
                    continue
        except MovieFolder.DoesNotExist:
            movie_folder = MovieFolder()
            movie_folder.inode = folder_dict[folder_name]
            movie_folder.name = folder_name
            movie_folder.save()

        movie_folders.append(movie_folder)
        if command_mode:
            print('updating movie media information')

        if root_folder:
            folder_full_name = os.path.join(root_folder, folder_name)
            if os.path.exists(folder_full_name):
                movie_files = [vf for vf in os.listdir(folder_full_name)
                               if os.path.isfile(os.path.join(folder_full_name, vf))
                               and vf.lower().endswith(('.mov', '.mp4', '.mkv', '.avi'))
                               and (os.path.getsize(os.path.join(folder_full_name, vf)) > 100000 or settings.DEBUG)]
                existing_folder_videos = FolderVideo.objects.filter(folder=movie_folder)
                existing_video_dict = {}
                for existing_folder_video in existing_folder_videos:
                    if existing_folder_video.name not in movie_files:
                        existing_folder_video.delete()
                    else:
                        existing_video_dict[existing_folder_video.name] = existing_folder_video
                for movie_file in movie_files:
                    if movie_file in existing_video_dict and not force_refresh:
                        continue

                    if movie_file in existing_video_dict:
                        folder_video = existing_video_dict[movie_file]
                    else:
                        folder_video = FolderVideo()

                    movie_file_full = os.path.join(folder_full_name, movie_file)

                    folder_video.inode = os.stat(movie_file_full).st_ino
                    folder_video.size = os.path.getsize(movie_file_full)
                    folder_video.folder = movie_folder
                    folder_video.name = movie_file

                    try:
                        media_info = MediaInfo.parse(movie_file_full)
                        folder_video.media_info = media_info.to_json()
                        if media_info.tracks:
                            folder_video.duration = media_info.tracks[0].duration
                            for track in media_info.tracks:
                                if track.track_type == 'Video':
                                    folder_video.height = track.height
                                    folder_video.width = track.width
                    except:
                        if command_mode:
                            print('Failed to get MediaInfo from {}'.format(movie_file_full))
                        logging.error('Failed to get MediaInfo from {}'.format(movie_file_full))

                    folder_video.save()

        name_year_dict = analyse_movie_folder(folder_name)
        m_name = name_year_dict[0]
        try:
            m_year = int(name_year_dict[1])
        except ValueError:
            m_year = -1

        if command_mode:
            print('updating movie information')

        movie_infos = search_tmdb_movie(m_name)
        if movie_infos:
            matching_movie_info = None
            for movie_info in movie_infos:

                release_date = movie_info['release_date']
                release_year = -1
                if release_date:
                    try:
                        release_year = int(release_date[:4])
                    except ValueError:
                        pass

                if not matching_movie_info:
                    matching_movie_info = movie_info
                    if release_year == m_year:
                        break
                else:
                    if release_year == m_year:
                        matching_movie_info = movie_info

            movie = save_movie(matching_movie_info, force_refresh)

            if movie:
                movie_folder.movie = movie
                movie_folder.save()
            else:
                if command_mode:
                    print("Fail to save movie list: {}".format(m_name))
                logging.warn("Fail to save movie list: {}".format(m_name))
        else:
            if command_mode:
                print("Can not found tmdb movie for keyword: {}".format(m_name))
            logging.warn("Can not found tmdb movie for keyword: {}".format(m_name))

    return movie_folders
