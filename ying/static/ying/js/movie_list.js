$(function () {
    $("#button_select_all").click(function () {
        $(".m_unchecked").prop('checked', true);
        $(".m_checked").prop('checked', false);
        $(".m_checkbox").toggleClass('m_checked');
        $(".m_checkbox").toggleClass('m_unchecked');
    });
    $("#check_box_select").click(function () {
        if ($("#check_box_select").prop('checked')){
            $(".m_checkbox").prop('checked', true);
        }
        else{
            $(".m_checkbox").prop('checked', false);
        }
    });
});
