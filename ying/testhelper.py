import os

MOVIE_SAMPLES = {
    'Lion': 2016,
    'La La Land': 2016,
    'Hell or High Water': 2016,
    'Hacksaw Ridge': 2016,
    'Arrival': 2016,
    'Moonlight': 2016,
    'Room': 2015,
    'Brooklyn': 2015,
    'Spotlight': 2015,
    'Birdman': 2015,
    'Boyhood': 2015,
    'Captain Phillips': 2014,
    'Dallas Buyers Club': 2014,
    'Gravity': 2014,
    'Her': 2013,
    'Philomena': 2013,
    'Lincoln': 2012,
    'Life of Pi': 2012,
    'Hugo': 2011,

}


def create_temp_movie_root(movie_folder, movie_count=5):
    if movie_count > len(MOVIE_SAMPLES):
        movie_count = len(MOVIE_SAMPLES)

    for i in range(0, movie_count):
        movie_name = MOVIE_SAMPLES.keys()[i]
        movie_year = MOVIE_SAMPLES[movie_name]
        movie_name = movie_name.replace(' ', '.')
        folder_name = movie_name + '.' + str(movie_year)
        os.makedirs(os.path.join(movie_folder, folder_name))

