from defaults import *

DEBUG = False
THUMBNAIL_DEBUG = False

ALLOWED_HOSTS = ['127.0.0.1', 'yichen1976.synology.me', '192.168.1.111']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'naszoodb',
        'USER': 'root',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '',
    }
}

MOVIE_ROOT = os.path.join(STATIC_ROOT, 'movie')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/var/www/naszoo/debug.log',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}
