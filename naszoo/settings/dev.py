import tempfile

from defaults import *
from ying.testhelper import create_temp_movie_root

DEBUG = True
THUMBNAIL_DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'naszoo_dev.sqlite3'),
    }
}

STATIC_ROOT = tempfile.mkdtemp()
print 'STATIC_ROOT: ' + STATIC_ROOT
MOVIE_ROOT = os.path.join(STATIC_ROOT, MOVIE_FOLDER)

create_temp_movie_root(MOVIE_ROOT)
