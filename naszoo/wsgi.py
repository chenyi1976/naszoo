"""
WSGI config for naszoo project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

try:
    import mod_wsgi

    os.environ['DJANGO_SETTINGS_MODULE'] = "naszoo.settings.production"
except ImportError:
    os.environ['DJANGO_SETTINGS_MODULE'] = "naszoo.settings.dev"

application = get_wsgi_application()
